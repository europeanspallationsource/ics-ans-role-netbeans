import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_netbeans_binary_checksum(host):
    assert host.file("/opt/netbeans-8.2/bin/netbeans").sha256sum == '55c38d4a25033d4416ffa2ea133f18a558db7e5ab2659e3c8a7d76c0fb26a48f'


def test_netbeans_binary_exists(host):
    assert host.file("/opt/netbeans-8.2/bin/netbeans").exists


def test_netbeans_binary_help(host):
    cmd = host.command("/opt/netbeans-8.2/bin/netbeans --help 2>&1")
    assert 'Usage:' in cmd.stdout
    assert 'General options:' in cmd.stdout
    assert 'Additional module options:' in cmd.stdout


def test_netbeans_jdkhome(host):
    netbeans_conf = host.file("/opt/netbeans-8.2/etc/netbeans.conf")
    # We don't check the minor version of the JDK (only 1.8)
    assert netbeans_conf.contains('netbeans_jdkhome="/usr/java/jdk1.8')
