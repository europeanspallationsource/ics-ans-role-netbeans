ics-ans-role-netbeans
=====================

Ansible role to install NetBeans in the Development Machine.

When I wrote this role I found very good inspiration for it here:
https://github.com/Oefenweb/ansible-netbeans-ide/

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
netbeans_ide_version: 8.2
netbeans_archive: https://artifactory.esss.lu.se/artifactory/swi-pkg/netbeans/{{ netbeans_ide_version }}/netbeans-8.2-201609300101.zip
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-netbeans
```

License
-------

BSD 2-clause
